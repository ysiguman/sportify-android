package com.sportify.sportify;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

/**
 * Created by ysiguman on 21/05/18.
 */

public class UserRessources {

    private static final String MyPREFERENCES = "MyPrefs" ;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;

    public UserRessources(Context context) {
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
    }

    public boolean userExist() { return !sharedpreferences.getAll().isEmpty(); }

    public void clear() {
        editor = sharedpreferences.edit();
        editor.clear();
        editor.apply();
    }

    public String getId() { return sharedpreferences.getString("id", null); }

    public void setId(String id) {
        editor = sharedpreferences.edit();
        editor.putString("id", id);
        editor.apply();
    }

    public String getUser() {
        return sharedpreferences.getString("user", null);
    }

    public void setUser(String user) {
        editor = sharedpreferences.edit();
        editor.putString("user", user);
        editor.apply();
    }

    public String getMail() {
        return sharedpreferences.getString("mail", null);
    }

    public void setMail(String mail) {
        editor = sharedpreferences.edit();
        editor.putString("mail", mail);
        editor.apply();
    }

    public void log() {
        Log.i("### USER ID ", getId());
        Log.i("### USER NAME ", getUser());
        Log.i("### USER MAIL ", getMail());
    }
}
