package com.sportify.sportify;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sportify.sportify.Model.Event;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by ysiguman on 15/05/18.
 */

public class EventListAdapter extends ArrayAdapter<Event> {
    UserRessources userRessources;

    public EventListAdapter(@NonNull Context context, List<Event> listEvent) {
        super(context, 0, listEvent);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        userRessources = new UserRessources(getContext());
        if (convertView ==null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_event, parent, false);
        }


        Event event = getItem(position);
        DateFormat dateFormat = new SimpleDateFormat("HH:mm - dd/MM/yyyy");

        EventViewHolder eventViewHolder = (EventViewHolder) convertView.getTag();
        if (eventViewHolder == null)
        {
            eventViewHolder = new EventViewHolder();
            eventViewHolder.name = convertView.findViewById(R.id.name);
            eventViewHolder.sport = convertView.findViewById(R.id.sport);
            eventViewHolder.date = convertView.findViewById(R.id.date);

            eventViewHolder.wait = convertView.findViewById(R.id.wait);
            eventViewHolder.member = convertView.findViewById(R.id.member);
            eventViewHolder.principal = convertView.findViewById(R.id.principal);

            eventViewHolder.nbMembers = convertView.findViewById(R.id.members);
            eventViewHolder.registered = convertView.findViewById(R.id.registered);
            eventViewHolder.localisation = convertView.findViewById(R.id.localisation);

        }

        eventViewHolder.name.setText(event.name);
        eventViewHolder.sport.setText(event.sport);
        eventViewHolder.date.setText(dateFormat.format(event.date));
        eventViewHolder.nbMembers.setText(String.valueOf(event.members));
        eventViewHolder.registered.setText(String.valueOf(event.registered));
        eventViewHolder.localisation.setText(event.localisation);

        if (userRessources.getId().equals(event.principal)) {
            eventViewHolder.principal.setVisibility(View.VISIBLE);
        } else if (event.state != null && event.state.equals("0")) {
            eventViewHolder.wait.setVisibility(View.VISIBLE);
        } else if (event.state != null && event.state.equals("1")) {
            eventViewHolder.member.setVisibility(View.VISIBLE);
        }

        return convertView;

    }

    private class EventViewHolder {
        public TextView name;
        public TextView sport;
        public TextView date;
        public TextView nbMembers;
        public TextView registered;
        public TextView localisation;

        public TextView wait;
        public TextView member;
        public TextView principal;
    }
}
