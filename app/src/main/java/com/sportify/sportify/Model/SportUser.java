package com.sportify.sportify.Model;

/**
 * Created by ysiguman on 23/05/18.
 */

public class SportUser {
    public String id;
    public String name;
    public String comment;
    public String level;

    public SportUser() {
    }

    public SportUser(String id, String name, String comment, String level) {
        this.id = id;
        this.name = name;
        this.comment = comment;
        this.level = level;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
