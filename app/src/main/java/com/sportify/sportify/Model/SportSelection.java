package com.sportify.sportify.Model;

/**
 * Created by ysiguman on 19/05/18.
 */

public class SportSelection {
    public String id;
    public String name;

    public SportSelection(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return this.name;
    }
}

