package com.sportify.sportify.Model;

import java.util.Date;

/**
 * Created by ysiguman on 15/05/18.
 */

public class User {
    public String id;
    public String name;
    public String mail;
    public String firstName;
    public String localisation;
    public String comment;
    public Date createdDate;

    public User() {
    }

    public User(String id,
                String name,
                String mail,
                String firstName,
                String localisation,
                String comment,
                Date createdDate) {
        this.id = id;
        this.name = name;
        this.mail = mail;
        this.firstName = firstName;
        this.localisation = localisation;
        this.comment = comment;
        this.createdDate = createdDate;
    }
}
