package com.sportify.sportify.Model;

/**
 * Created by ysiguman on 22/05/18.
 */

public class UserListEvent {
    private String id;
    private String name;
    private String state;

    public UserListEvent(String id, String name, String state) {
        this.id = id;
        this.name = name;
        this.state = state;
    }

    public UserListEvent() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
