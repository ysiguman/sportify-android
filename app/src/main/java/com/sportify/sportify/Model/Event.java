package com.sportify.sportify.Model;

import java.util.Date;

/**
 * Created by ysiguman on 15/05/18.
 */

public class Event {
    public String id;
    public String name;
    public String sport;
    public String level;
    public Integer members;
    public String comment;
    public Date date;
    public String principal;
    public String state;
    public String registered;
    public String localisation;

    public Event() {
    }

    public Event(String id, String name, String sport, String level, Integer members, String comment, Date date, String principal, String state, String registered, String localisation) {
        this.id = id;
        this.name = name;
        this.sport = sport;
        this.level = level;
        this.members = members;
        this.comment = comment;
        this.date = date;
        this.principal = principal;
        this.state = state;
        this.registered = registered;
        this.localisation = localisation;
    }
}
