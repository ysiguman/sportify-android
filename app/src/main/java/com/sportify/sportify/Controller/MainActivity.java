package com.sportify.sportify.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.sportify.sportify.UserRessources;

public class MainActivity extends AppCompatActivity {
    UserRessources userRessources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();

        userRessources = new UserRessources(this);
        if (userRessources.userExist()) {
            Toast.makeText(MainActivity.this,
                    userRessources.getUser() + "\n" + userRessources.getMail(),
                    Toast.LENGTH_SHORT);
            Intent intent = new Intent(MainActivity.this, MainSearchActivity.class);
            startActivity(intent);
        } else {
            Intent intent = new Intent(MainActivity.this, ConnectionActivity.class);
            startActivity(intent);
        }
    }
}
