package com.sportify.sportify.Controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sportify.sportify.Model.SportUser;
import com.sportify.sportify.Model.User;
import com.sportify.sportify.R;
import com.sportify.sportify.Service.UserInterface;
import com.sportify.sportify.UserRessources;
import com.sportify.sportify.UserSportAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ysiguman on 23/05/18.
 */

public class ProfileActivity extends AppCompatActivity {
    UserRessources userRessources;
    UserInterface userInterface;
    User user;
    List<SportUser> sports;
    Button addSportBtn;

    RecyclerView recyclerView;
    UserSportAdapter userSportAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profil);


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        userRessources = new UserRessources(this);
        userInterface = APIClient.getClient().create(UserInterface.class);

        sports = new ArrayList<>();

        String id = getIntent().getStringExtra("ID_USER");

        getDatas(id);

        recyclerView = findViewById(R.id.sportList);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(ProfileActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        userSportAdapter = new UserSportAdapter(this, sports);
        recyclerView.setAdapter(userSportAdapter);

        addSportBtn = findViewById(R.id.addSport);
    }

    private void getDatas(String id) {

        userInterface.getUser(id).enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                user = new User(
                        response.body().get(0).id,
                        response.body().get(0).name,
                        response.body().get(0).mail,
                        response.body().get(0).firstName,
                        response.body().get(0).localisation,
                        response.body().get(0).comment,
                        response.body().get(0).createdDate
                );

                setData();
                getSports(id);
            }

            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                Log.d("__________", t.getMessage());
            }
        });
    }


    private void getSports(String id) {
        sports.clear();
        userInterface.getUserSports(id).enqueue(new Callback<List<SportUser>>() {
            @Override
            public void onResponse(Call<List<SportUser>> call, Response<List<SportUser>> response) {
                for (SportUser sport : response.body()) {
                    sports.add(new SportUser(
                            sport.id,
                            sport.name,
                            sport.comment,
                            sport.level
                    ));
                }
                userSportAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<SportUser>> call, Throwable t) {
                Log.d("__________", t.getMessage());
            }
        });
    }

    private void setData() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");

        TextView name = findViewById(R.id.name);
        TextView firstName = findViewById(R.id.first_name);
        TextView mail = findViewById(R.id.mail);
        TextView createdDate = findViewById(R.id.created_date);
        TextView comment = findViewById(R.id.comment);
        TextView localisation = findViewById(R.id.localisation);

        name.setText(user.name);
        mail.setText(user.mail);
        firstName.setText(user.firstName);
        localisation.setText(user.localisation);
        createdDate.setText(simpleDateFormat.format(user.createdDate));
        comment.setText(user.comment);


        if (userRessources.getId().equals(user.id)) {
            addSportBtn.setVisibility(View.VISIBLE);

            addSportBtn.setOnClickListener(view -> {
                Intent intent = new Intent(ProfileActivity.this, UserAddSport.class);
                startActivityForResult(intent, 1);
            });
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                getDatas(user.id);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}
