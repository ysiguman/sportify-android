package com.sportify.sportify.Controller;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sportify.sportify.Model.Event;
import com.sportify.sportify.Model.SportSelection;
import com.sportify.sportify.R;
import com.sportify.sportify.Service.EventInterface;
import com.sportify.sportify.Service.RessourceInterface;
import com.sportify.sportify.UserRessources;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ysiguman on 19/05/18.
 */

public class EventNew extends AppCompatActivity
        implements Toolbar.OnMenuItemClickListener,
        TimePickerDialog.OnTimeSetListener,
        DatePickerDialog.OnDateSetListener {
    RessourceInterface ressourceInterface;
    EventInterface eventInterface;
    UserRessources userRessources;

    Event event;
    List<SportSelection> sports;

    EditText datePicker;
    EditText timePicker;
    Calendar myCalendar;
    Button sendBtn;

    String[] levels = new String[]{"Débutant", "Intermédiaire", "Confirmé"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_new);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());
        toolbar.inflateMenu(R.menu.new_event);
        toolbar.setOnMenuItemClickListener(this);

        ressourceInterface = APIClient.getClient().create(RessourceInterface.class);
        eventInterface = APIClient.getClient().create(EventInterface.class);
        userRessources = new UserRessources(this);

        sports = new ArrayList<>();
        Spinner sportList = findViewById(R.id.spinnerSport);
        ArrayAdapter<SportSelection> sportArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sports);
        sportList.setAdapter(sportArrayAdapter);

        Spinner levelList = findViewById(R.id.spinnerLevel);
        ArrayAdapter<String> levelAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, levels);
        levelList.setAdapter(levelAdapter);

        Call<List<SportSelection>> call = ressourceInterface.getSports();

        call.enqueue(new Callback<List<SportSelection>>() {
            @Override
            public void onResponse(Call<List<SportSelection>> call, Response<List<SportSelection>> response) {
                for (SportSelection sport: response.body()) {
                    sports.add(new SportSelection(sport.id,
                            sport.name));
                }
                sportArrayAdapter.notifyDataSetChanged();

            }


            @Override
            public void onFailure(Call<List<SportSelection>> call, Throwable t) {

            }

        });

        myCalendar = Calendar.getInstance();
        datePicker = findViewById(R.id.datePicker);
        timePicker = findViewById(R.id.timePicker);

        datePicker.setOnClickListener(view -> new DatePickerDialog(EventNew.this, EventNew.this, myCalendar.get(Calendar.YEAR),
                myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show());

        timePicker.setOnClickListener(view -> new TimePickerDialog(EventNew.this, EventNew.this, myCalendar.get(Calendar.HOUR),
                myCalendar.get(Calendar.MINUTE), true).show());

        sendBtn = findViewById(R.id.sendBtn);
        sendBtn.setOnClickListener(view -> getEvent());

    }

    private void updateLabelDate() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.FRANCE);

        datePicker.setText(sdf.format(myCalendar.getTime()));
    }

    private void updateLabelTime() {
        String myFormat = "HH:mm"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.FRANCE);

        timePicker.setText(sdf.format(myCalendar.getTime()));
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                getEvent();
                return true;
        }

        return true;
    }

    private void getEvent() {
        boolean error = false;
        event = new Event();
        EditText name = findViewById(R.id.name);
        EditText nbMembers = findViewById(R.id.members);
        EditText comment = findViewById(R.id.comment);
        Spinner sport = findViewById(R.id.spinnerSport);
        Spinner level = findViewById(R.id.spinnerLevel);

        if (name.getText().toString().trim().equalsIgnoreCase("")) {
            name.setError("Veuillez renseigner un Nom");
            error = true;
        }

        if (datePicker.getText().toString().trim().equalsIgnoreCase("")) {
            datePicker.setError("Veillez renseigner une date");
            error = true;
        } else if (Calendar.getInstance().after(myCalendar.getTime())) {
            datePicker.setError("Veuillez sélectionner une date supérieur");
            error = true;
        }

        if (timePicker.getText().toString().trim().equalsIgnoreCase("")) {
            timePicker.setError("Veillez renseigner un horraire");
            error = true;
        } else if (Calendar.getInstance().after(myCalendar.getTime())) {
            timePicker.setError("Veuillez sélectionner un horraire supérieur");
            error = true;
        }
        if (nbMembers.getText().toString().trim().equalsIgnoreCase("")) {
            name.setError("Veuillez renseigner un nombre de membre");
            error = true;
        }

        if (error) {
            Toast.makeText(EventNew.this, "Veillez renseigner les champs correctement", Toast.LENGTH_SHORT).show();
        } else {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm - dd/MM/yyyy");

            event.name = name.getText().toString();
            event.members = Integer.valueOf(nbMembers.getText().toString());
            event.comment = comment.getText().toString();
            event.sport = sport.getSelectedItem().toString();
            event.level = level.getSelectedItem().toString();
            event.date = myCalendar.getTime();

            eventInterface.createEvent(event, userRessources.getId()).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(EventNew.this, "Event Created", Toast.LENGTH_SHORT).show();
                        Log.i("###", "post submitted to API." + response.body().toString());
                        setResult(RESULT_OK);
                        finish();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("Error", "Unable to submit post to API.");
                }
            });
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        updateLabelDate();
    }

    @Override
    public void onTimeSet(TimePicker timePicker, int hour, int minute) {
        myCalendar.set(Calendar.HOUR, hour);
        myCalendar.set(Calendar.MINUTE, minute);
        updateLabelTime();
    }
}
