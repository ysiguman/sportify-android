package com.sportify.sportify.Controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sportify.sportify.Model.SportSelection;
import com.sportify.sportify.Model.SportUser;
import com.sportify.sportify.R;
import com.sportify.sportify.Service.RessourceInterface;
import com.sportify.sportify.Service.UserInterface;
import com.sportify.sportify.UserRessources;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ysiguman on 23/05/18.
 */

public class UserAddSport extends AppCompatActivity {
    List<SportSelection> sports;
    SportUser sport;
    RessourceInterface ressourceInterface;
    UserRessources userRessources;
    UserInterface userInterface;
    String[] levels = new String[]{"Débutant", "Intermédiaire", "Confirmé"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sport_user_new);

        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        userRessources = new UserRessources(this);
        ressourceInterface = APIClient.getClient().create(RessourceInterface.class);
        userInterface = APIClient.getClient().create(UserInterface.class);

        sports = new ArrayList<>();
        sport = new SportUser();

        Spinner sportList = findViewById(R.id.spinnerSport);
        ArrayAdapter<SportSelection> sportArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sports);
        sportList.setAdapter(sportArrayAdapter);


        Spinner levelList = findViewById(R.id.spinnerLevel);
        ArrayAdapter<String> levelAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, levels);
        levelList.setAdapter(levelAdapter);

        Call<List<SportSelection>> call = ressourceInterface.getSports();

        call.enqueue(new Callback<List<SportSelection>>() {
            @Override
            public void onResponse(Call<List<SportSelection>> call, Response<List<SportSelection>> response) {
                for (SportSelection sport: response.body()) {
                    sports.add(new SportSelection(sport.id,
                            sport.name));
                }
                sportArrayAdapter.notifyDataSetChanged();

            }


            @Override
            public void onFailure(Call<List<SportSelection>> call, Throwable t) {
                Log.e("### ERROR", t.toString());
            }

        });

        Button addBtn = findViewById(R.id.sendBtn);
        addBtn.setOnClickListener(view -> addSport());
    }

    public void addSport() {
        Spinner sportSpinner = findViewById(R.id.spinnerSport);
        Spinner levelSpinner = findViewById(R.id.spinnerLevel);
        EditText comment = findViewById(R.id.comment);

        sport.comment = comment.getText().toString();
        sport.name = sportSpinner.getSelectedItem().toString();
        sport.level = levelSpinner.getSelectedItem().toString();

        userInterface.addSport(userRessources.getId(), sport).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Toast.makeText(UserAddSport.this, "Sport Created", Toast.LENGTH_SHORT).show();
                Log.i("###", "post submitted to API." + response.body().toString());
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("### ERROR ", t.toString());
            }
        });
    }
}
