package com.sportify.sportify.Controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.sportify.sportify.Model.CurrentUser;
import com.sportify.sportify.R;
import com.sportify.sportify.Service.UserInterface;
import com.sportify.sportify.UserRessources;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ysiguman on 23/05/18.
 */

public class CreateUserActivity extends AppCompatActivity {
    Button createBtn;
    Button backBtn;
    EditText mailEditText;
    EditText passEditText;
    EditText nameEditText;

    CurrentUser user;
    UserInterface userInterface;

    UserRessources userRessources;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_user);

        ImageView imageView = findViewById(R.id.logo);
        imageView.setImageResource(R.drawable.sportify);

        userInterface = APIClient.getClient().create(UserInterface.class);
        userRessources = new UserRessources(this);

        user = new CurrentUser();

        createBtn = findViewById(R.id.createBtn);
        backBtn = findViewById(R.id.backBtn);

        mailEditText = findViewById(R.id.mail);
        passEditText = findViewById(R.id.password);
        nameEditText = findViewById(R.id.name);

        backBtn.setOnClickListener(view -> finish());
        createBtn.setOnClickListener(view -> getDatas());
    }

    public void getDatas() {
        boolean error = false;

        if (nameEditText.toString().trim().equalsIgnoreCase("")) {
            nameEditText.setError("Veuillez renseigner un Nom");
            error = true;
        }

        if (mailEditText.toString().trim().equalsIgnoreCase("")) {
            mailEditText.setError("Veuillez renseigner un Mail");
            error = true;
        }

        if (passEditText.toString().trim().equalsIgnoreCase("")) {
            passEditText.setError("Veuillez renseigner un mot de pass");
            error = true;
        }

        if (error) {
            Toast.makeText(CreateUserActivity.this, "Veillez renseigner les champs correctement", Toast.LENGTH_SHORT).show();
        } else {
            user.setName(nameEditText.getText().toString());
            user.setMail(mailEditText.getText().toString());
            user.setPassword(passEditText.getText().toString());

            userInterface.createUser(user).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.isSuccessful()) {
                        Toast.makeText(CreateUserActivity.this, "User Created", Toast.LENGTH_SHORT).show();
                        Log.i("###", "post submitted to API." + response.body().toString());

                        finish();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("### ERROR ", t.toString());

                }
            });
        }
    }
}
