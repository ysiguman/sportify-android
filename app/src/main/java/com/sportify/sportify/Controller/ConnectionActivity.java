package com.sportify.sportify.Controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.sportify.sportify.Model.CurrentUser;
import com.sportify.sportify.R;
import com.sportify.sportify.Service.UserInterface;
import com.sportify.sportify.UserRessources;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ysiguman on 21/05/18.
 */

public class ConnectionActivity extends AppCompatActivity {
    Button sendBtn;
    Button buttonCreate;
    EditText mailEditText;
    EditText passEditText;

    CurrentUser user;
    UserInterface userInterface;

    UserRessources userRessources;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_screen);

        ImageView imageView = findViewById(R.id.logo);
        imageView.setImageResource(R.drawable.sportify);

        userInterface = APIClient.getClient().create(UserInterface.class);
        userRessources = new UserRessources(this);

        user = new CurrentUser();

        sendBtn = findViewById(R.id.button);
        buttonCreate = findViewById(R.id.buttonCreate);
        mailEditText = findViewById(R.id.mail);
        passEditText = findViewById(R.id.password);

        sendBtn.setOnClickListener(view -> {
            user.setMail(mailEditText.getText().toString());
            user.setPassword(passEditText.getText().toString());

            if (user.getMail().trim().equalsIgnoreCase("")) {
                mailEditText.setError("Veuillez renseigner un Nom");
            }
            if (user.getPassword().trim().equalsIgnoreCase("")) {
                passEditText.setError("Veuillez renseigner un Nom");
            }

            getUser();
        });

        buttonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ConnectionActivity.this, CreateUserActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getUser() {
        userInterface.connect(user).enqueue(new Callback<CurrentUser>() {
            @Override
            public void onResponse(Call<CurrentUser> call, Response<CurrentUser> response) {
                Log.i("### ID ", response.body().getId());
                Log.i("### NAME ", response.body().getName());
                Log.i("### MAIL ", response.body().getMail());

                connectUser(response.body());
            }

            @Override
            public void onFailure(Call<CurrentUser> call, Throwable t) {
                Log.e("### ERROR ", "L'utilisateur n'existe pas");
                Log.e("### ERROR ", t.toString());
            }
        });
    }

    private void connectUser(CurrentUser user) {
        if (userRessources.userExist()) {
            userRessources.clear();
        }

        userRessources.setId(user.getId());
        userRessources.setUser(user.getName());
        userRessources.setMail(user.getMail());

        userRessources.log();

        Intent intent = new Intent(ConnectionActivity.this, MainSearchActivity.class);
        finish();
        startActivity(intent);
    }
}
