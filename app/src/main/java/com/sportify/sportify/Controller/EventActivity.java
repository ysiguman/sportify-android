package com.sportify.sportify.Controller;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sportify.sportify.Model.Event;
import com.sportify.sportify.Model.UserListEvent;
import com.sportify.sportify.R;
import com.sportify.sportify.Service.EventInterface;
import com.sportify.sportify.UserAdapter;
import com.sportify.sportify.UserRessources;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventActivity extends AppCompatActivity {
    EventInterface eventInterface;
    UserRessources userRessources;

    UserAdapter userAdapter;

    public Event event;
    DateFormat dateFormat;
    TextView text;
    TextView date;
    TextView comment;
    TextView sport;
    TextView localisation;
    TextView registered;
    TextView members;
    List<UserListEvent> users;
    Button reserve;
    Button unReserve;
    Button delete;

    @SuppressLint("SimpleDateFormat")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        dateFormat = new SimpleDateFormat("HH:mm - dd/MM/yyyy");
        users = new ArrayList<>();

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        eventInterface = APIClient.getClient().create(EventInterface.class);
        userRessources = new UserRessources(this);
        event = new Event();

        String id = getIntent().getStringExtra("ID_USER");

        RecyclerView recyclerView = findViewById(R.id.usersList);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(EventActivity.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(horizontalLayoutManagaer);
        userAdapter = new UserAdapter(EventActivity.this, users);
        recyclerView.setAdapter(userAdapter);

        text = findViewById(R.id.title);
        date = findViewById(R.id.date);
        comment = findViewById(R.id.comment);
        sport = findViewById(R.id.sport);
        localisation = findViewById(R.id.localisation);
        members = findViewById(R.id.members);
        registered = findViewById(R.id.registered);

        getDatas(id);


        reserve = findViewById(R.id.reserve);
        unReserve = findViewById(R.id.unReserve);
        delete = findViewById(R.id.delete);

        reserve.setOnClickListener(view -> eventInterface.addUser(userRessources.getId(), event.id)
                .enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                getUsers(event.id);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("##### Error :", t.toString());
            }

        }));

        unReserve.setOnClickListener(view -> eventInterface.removeUser(userRessources.getId(), event.id)
                .enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                getUsers(event.id);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("##### Error :", t.toString());
            }

        }));

        delete.setOnClickListener(view -> eventInterface.deleteEvent(event.id)
                .enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                setResult(RESULT_OK);
                finish();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("##### Error :", t.toString());
            }

        }));
    }

    private void getDatas (String id) {
        Call<List<Event>> call = eventInterface.getEvent(id);

        call.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                event = response.body().get(0);
                text.setText(event.name);
                date.setText(dateFormat.format(event.date));
                comment.setText(event.comment);
                sport.setText(event.sport);
                localisation.setText(event.localisation);
                members.setText(String.valueOf(event.members));
                registered.setText(String.valueOf(event.registered));

                getUsers(id);
            }


            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Log.d("__________", t.getMessage());
            }

        });
    }

    private void getUsers (String id) {
        users.clear();
        eventInterface.getUsers(id).enqueue(new Callback<List<UserListEvent>>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onResponse(Call<List<UserListEvent>> call, Response<List<UserListEvent>> response) {
                for (UserListEvent userListEvent: response.body()) {
                    users.add(new UserListEvent(
                            userListEvent.getId(),
                            userListEvent.getName(),
                            userListEvent.getState()
                    ));
                }
                userAdapter.notifyDataSetChanged();
                setUserActions();
            }

            @Override
            public void onFailure(Call<List<UserListEvent>> call, Throwable t) {
                Log.e("### ERROR ", t.toString());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                Intent homeIntent = new Intent(this, MainActivity.class);
                homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    public void setUserActions() {
        Long countUser = users.stream().filter((el) -> el.getId()
                .equals(userRessources.getId()))
                .count();

        if (userRessources.getId().equals(event.principal)) {
            delete.setVisibility(View.VISIBLE);
        } else {
            if (countUser == 0L) {
                reserve.setVisibility(View.VISIBLE);
                unReserve.setVisibility(View.GONE);
            } else if (countUser == 1L) {
                reserve.setVisibility(View.GONE);
                unReserve.setVisibility(View.VISIBLE);
            }
        }
    }

    public void acceptUser(String id) {
        eventInterface.acceptUser(id, event.id)
        .enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                getDatas(event.id);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("##### Error :", t.toString());
            }

        });
    }

    public void revocUser(String id) {
        eventInterface.revocUser(id, event.id)
            .enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    getDatas(event.id);
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("##### Error :", t.toString());
                }

            });
    }

    public String getPrincipal() {
        return event.principal;
    }

}
