package com.sportify.sportify.Controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.DividerDrawerItem;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.SecondaryDrawerItem;
import com.sportify.sportify.EventListAdapter;
import com.sportify.sportify.Model.Event;
import com.sportify.sportify.R;
import com.sportify.sportify.Service.EventInterface;
import com.sportify.sportify.UserRessources;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ysiguman on 21/05/18.
 */

public class MainSearchActivity extends AppCompatActivity {

    private EventInterface eventInterface;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ListView listView;
    private List<Event> events;
    private List<Event> listEventsSelected;
    private List<View> listRowsSelected;
    private EventListAdapter eventListAdapter;
    private MenuItem menuItemDelete;
    private UserRessources userRessources;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        eventInterface = APIClient.getClient().create(EventInterface.class);
        userRessources = new UserRessources(this);

        events = new ArrayList<>();
        listEventsSelected = new ArrayList<>();
        listRowsSelected = new ArrayList<>();

        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this::getDatas);

        createDrawer();
        getDatas();

        eventListAdapter = new EventListAdapter(MainSearchActivity.this, events);

        listView = findViewById(R.id.listView);
        listView.setAdapter(eventListAdapter);
        listView.setOnItemClickListener((adapterView, view, i, l) -> {
            Event event = (Event) listView.getItemAtPosition(i);
            Intent appInfo = new Intent(MainSearchActivity.this, EventActivity.class);
            appInfo.putExtra("ID_USER", event.id);
            startActivityForResult(appInfo, 1);
        });

        listView.setOnItemLongClickListener((parent, view, pos, id) -> {
            if (listRowsSelected.contains(view)) {
                listRowsSelected.remove(view);
                listEventsSelected.remove(events.get(pos));
                view.setBackgroundResource(R.color.md_light_background);
            } else {
                listEventsSelected.add(events.get(pos));
                listRowsSelected.add(view);
                view.setBackgroundResource(R.color.colorSelectedEl);
            }
            if(listRowsSelected.size() > 0){
                showDeleteMenu(true);
            }else{
                showDeleteMenu(false);
            }

            return true;
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(view -> {
            Intent appInfo = new Intent(MainSearchActivity.this, EventNew.class);
            startActivityForResult(appInfo, 1);
        });
    }

    private void createDrawer() {
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .withHeaderBackground(R.color.accent)
                .addProfiles(
                        new ProfileDrawerItem().withName(userRessources.getUser()).withEmail(userRessources.getMail())
                                .withIcon(getResources()
                                .getDrawable(R.drawable.ic_launcher_background))
                )
                .build();

        DrawerBuilder myDrawer = new DrawerBuilder()
                .withActivity(this)
                .withAccountHeader(headerResult);

        SecondaryDrawerItem home = new SecondaryDrawerItem().withIdentifier(1).withName("Liste Event");
        SecondaryDrawerItem newEvent = new SecondaryDrawerItem().withIdentifier(2).withName("Nouvel Event");
        SecondaryDrawerItem profile = new SecondaryDrawerItem().withIdentifier(3).withName("Profile");
        PrimaryDrawerItem disconnect = new PrimaryDrawerItem().withIdentifier(4).withName("Disconnect").withSelectable(false);
        myDrawer.addDrawerItems(
                home,
                newEvent,
                profile,
                new DividerDrawerItem(),
                disconnect
        ).withOnDrawerItemClickListener((view, position, drawerItem) -> {
            Intent appInfo;
            switch ((int) drawerItem.getIdentifier()) {
                case 1:
                    Log.i("### ", "Go home");
                    break;
                case 2:
                    Log.i("### ", "New Event");
                    appInfo = new Intent(MainSearchActivity.this, EventNew.class);
                    startActivityForResult(appInfo, 1);
                    break;
                case 3:
                    Log.i("### ", "Profile");
                    appInfo = new Intent(MainSearchActivity.this, ProfileActivity.class);
                    appInfo.putExtra("ID_USER", userRessources.getId());
                    startActivity(appInfo);
                    break;
                case 4:
                    Log.i("### ", "disconnect");
                    userRessources.clear();
                    finish();
                    break;
            }
            return true;
        });

        myDrawer.build();
    }


    private void getDatas () {
        Call<List<Event>> call = eventInterface.getEventsForUser(userRessources.getId());
        events.clear();

        call.enqueue(new Callback<List<Event>>() {
            @Override
            public void onResponse(Call<List<Event>> call, Response<List<Event>> response) {
                for (Event event: response.body()) {
                    events.add(new Event(event.id,
                            event.name,
                            event.sport,
                            event.level,
                            event.members,
                            event.comment,
                            event.date,
                            event.principal,
                            event.state,
                            event.registered,
                            event.localisation));
                }

                eventListAdapter.notifyDataSetChanged();

                mSwipeRefreshLayout.setRefreshing(false);
            }


            @Override
            public void onFailure(Call<List<Event>> call, Throwable t) {
                Log.e("##### Error :", t.toString());
            }

        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                getDatas();
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_toolbar, menu);
        menuItemDelete = menu.findItem(R.id.action_delete);

        menuItemDelete.setVisible(false);

        menuItemDelete.setOnMenuItemClickListener(item -> {
            deleteEvents();
            return true;
        });

        return true;

    }

    private void showDeleteMenu(boolean show){
        menuItemDelete.setVisible(show);
    }

    private void deleteEvents() {
        for (Event event: listEventsSelected) {
            Log.i("###### Event : ", event.id);

            eventInterface.deleteEvent(event.id).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    events.remove(event);
                    listEventsSelected.remove(event);

                    eventListAdapter.notifyDataSetChanged();
                    for (View view: listRowsSelected) {
                        view.setBackgroundResource(R.color.colorSelectedEl);
                    }
                    listRowsSelected.clear();
                }


                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e("##### Error :", t.toString());
                }

            });
        }
    }
}
