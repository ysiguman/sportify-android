package com.sportify.sportify;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportify.sportify.Model.SportUser;

import java.util.List;

/**
 * Created by ysiguman on 23/05/18.
 */

public class SportUserAdapter extends RecyclerView.Adapter<SportUserAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater mInflater;
    private List<SportUser> sports;

    public SportUserAdapter(Context context, List<SportUser> sports) {
        this.context = context;
        mInflater = LayoutInflater.from(context);
        this.sports = sports;
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_sport_user, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SportUser sport = sports.get(position);
        holder.name.setText(sport.name);
        holder.level.setText(String.valueOf(sport.level));
        holder.comment.setText(sport.comment);
    }

    @Override
    public int getItemCount() {
        return sports.size();
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        public TextView level;
        public TextView comment;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            level = itemView.findViewById(R.id.level);
            comment = itemView.findViewById(R.id.comment);
        }
    }
}
