package com.sportify.sportify;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportify.sportify.Controller.EventActivity;
import com.sportify.sportify.Controller.ProfileActivity;
import com.sportify.sportify.Model.UserListEvent;

import java.util.List;

/**
 * Created by ysiguman on 22/05/18.
 */

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {
    Context context;
    private LayoutInflater mInflater;
    private UserRessources userRessources;
    private List<UserListEvent> users;
    private UserListEvent userListEvent;

    public UserAdapter(Context context, List<UserListEvent> users) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.users = users;

        userRessources = new UserRessources(context);
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_user, parent, false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        userListEvent = users.get(position);
        holder.id.setText(userListEvent.getId());
        holder.name.setText(userListEvent.getName());
        holder.state.setText(userListEvent.getState().equals("0") ?
            "En Attente" : "Participant");
        holder.setListener();
        holder.setColor();
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener  {
        public TextView id;
        public TextView state;
        public TextView name;
        public View bottom;
        public String stateType;

        ViewHolder(View itemView) {
            super(itemView);
            id = itemView.findViewById(R.id.id);
            state = itemView.findViewById(R.id.state);
            name = itemView.findViewById(R.id.name);
            bottom = itemView.findViewById(R.id.bottom);
        }

        @Override
        public void onClick(View view) {
            Intent appInfo = new Intent(context, ProfileActivity.class);
            appInfo.putExtra("ID_USER", id.getText().toString());
            context.startActivity(appInfo);
        }

        @Override
        public boolean onLongClick(View view) {
           AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle(stateType.equals("0") ?
                "Accepter ": "Revoquer ");
            builder.setMessage(name.getText().toString());
            builder.setPositiveButton("Oui", (dialogInterface, i) -> {

                if (stateType.equals("0")) {
                    ((EventActivity)context).acceptUser(id.getText().toString());
                } else {
                    ((EventActivity)context).revocUser(id.getText().toString());
                }
                dialogInterface.cancel();
            });
            builder.setNegativeButton("Non", (dialogInterface, i) -> dialogInterface.cancel());

            AlertDialog dialog = builder.create();
            dialog.show();

            return false;
        }

        public void setListener() {
            itemView.setOnClickListener(this);

            stateType = userListEvent.getState();
            if (userRessources.getId().equals(((EventActivity)context).getPrincipal()) &&
                    !userListEvent.getId().equals(((EventActivity)context).getPrincipal())) {
                itemView.setOnLongClickListener(this);
            } else {
                itemView.setOnLongClickListener(null);
            }
        }

        public void setColor() {
            if (userListEvent.getId().equals(((EventActivity)context).getPrincipal())) {
                bottom.setBackgroundResource(R.color.primary);
            } else if (stateType != null && stateType.equals("0")) {
                bottom.setBackgroundColor(Color.parseColor("#888888"));
            } else if (stateType != null && stateType.equals("1")) {
                bottom.setBackgroundResource(R.color.colorAccent);
            }

        }
    }



}
