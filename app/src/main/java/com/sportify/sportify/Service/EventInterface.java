package com.sportify.sportify.Service;

import com.sportify.sportify.Model.Event;
import com.sportify.sportify.Model.UserListEvent;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by ysiguman on 15/05/18.
 */

public interface EventInterface {
    @GET("/api/events")
    Call<List<Event>> getEvents();


    @GET("/api/users/{id}/allEvents")
    Call<List<Event>> getEventsForUser(@Path("id") String id);

    @GET("/api/events/{id}")
    Call<List<Event>> getEvent(@Path("id") String id);

    @Multipart
    @POST("/api/events")
    Call<ResponseBody> createEvent(
            @Part("event") Event event,
            @Part("userId") String id);

    @DELETE("/api/events/{id}")
    Call<ResponseBody> deleteEvent(@Path("id") String id);

    @GET("/api/events/{id}/users")
    Call<List<UserListEvent>> getUsers(@Path("id") String id);

    @Multipart
    @POST("/api/events/{id}/add-user")
    Call<ResponseBody> addUser(
            @Part("user_id") String user_id,
            @Path("id") String idEvent);

    @Multipart
    @POST("/api/events/{id}/remove-user")
    Call<ResponseBody> removeUser(
            @Part("user_id") String user_id,
            @Path("id") String idEvent);


    @Multipart
    @POST("/api/events/{id}/accept-user")
    Call<ResponseBody> acceptUser(
            @Part("user_id") String user_id,
            @Path("id") String idEvent);

    @Multipart
    @POST("/api/events/{id}/revoc-user")
    Call<ResponseBody> revocUser(
            @Part("user_id") String user_id,
            @Path("id") String idEvent);
}
