package com.sportify.sportify.Service;

import com.sportify.sportify.Model.CurrentUser;
import com.sportify.sportify.Model.SportUser;
import com.sportify.sportify.Model.User;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by ysiguman on 15/05/18.
 */

public interface UserInterface {
    @GET("/api/users")
    Call<List<User>> getUsers();


    @POST("/api/users")
    Call<ResponseBody> createUser(@Body CurrentUser user);

    @GET("/api/users/{id}")
    Call<List<User>> getUser(@Path("id") String id);

    @GET("/api/users/{id}/sports")
    Call<List<SportUser>> getUserSports(@Path("id") String id);

    @POST("/api/users/connect")
    Call<CurrentUser> connect(@Body CurrentUser user);

    @POST("/api/users/{id}/sports")
    Call<ResponseBody> addSport(
            @Path("id") String id,
            @Body SportUser sport);

}
