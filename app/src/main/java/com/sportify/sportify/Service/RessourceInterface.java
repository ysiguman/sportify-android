package com.sportify.sportify.Service;


import com.sportify.sportify.Model.SportSelection;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by ysiguman on 19/05/18.
 */

public interface RessourceInterface {
    @GET("/api/sports")
    Call<List<SportSelection>> getSports();
}
