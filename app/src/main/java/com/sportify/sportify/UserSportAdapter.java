package com.sportify.sportify;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sportify.sportify.Model.SportUser;

import java.util.List;

/**
 * Created by ysiguman on 23/05/18.
 */

public class UserSportAdapter extends RecyclerView.Adapter<UserSportAdapter.ViewHolder> {
    Context context;
    private LayoutInflater mInflater;
    private UserRessources userRessources;
    private List<SportUser> sports;
    private SportUser sport;

    public UserSportAdapter(Context context, List<SportUser> sports) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        this.sports = sports;

        userRessources = new UserRessources(context);
    }

    @Override
    @NonNull
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_sport_user, parent, false);
        return new ViewHolder(view);
    }



    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        sport = sports.get(position);

        Log.i("### view", sport.level);
        holder.name.setText(sport.getName());
        holder.level.setText(String.valueOf(sport.getLevel()));
        holder.comment.setText(sport.getComment());
    }

    @Override
    public int getItemCount() {
        return sports.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener  {
        public TextView name;
        public TextView level;
        public TextView comment;
        public View bottom;

        ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            level = itemView.findViewById(R.id.level);
            comment = itemView.findViewById(R.id.comment);
        }

        @Override
        public void onClick(View view) {
            //Intent appInfo = new Intent(context, ProfileActivity.class);
            //appInfo.putExtra("ID_USER", name.getText().toString());
            //context.startActivity(appInfo);
        }

        @Override
        public boolean onLongClick(View view) {


            return false;
        }
    }



}
